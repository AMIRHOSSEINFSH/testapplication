package com.example.testapplication.first;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testapplication.R;

public class secondActivity extends AppCompatActivity {

    ImageView imageView;
    Button    btn_edit;
    Button    button;
    TextView  textView;
    View      view;
    String message="Happy new Git arrival!!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        btn_edit = findViewById(R.id.btnChange);
        textView = findViewById(R.id.tv_main_fullName);
        button=findViewById(R.id.button);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("http://7learn.com"));
                startActivity(intent);
            }
        });




        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(secondActivity.this, DetailActivity.class);
                intent.putExtra("fullName",textView.getText().toString());
                startActivityForResult(intent, 1001);
            }
        });
    }


    // TODO: onActivityResult method is a CallBack  that wait for the Activity that come to this Activity and get result from it
    // todo: (just use startActivityForResult in source and setResult in destination)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //todo: data(intent in DetailActivity) is the intent that is called in source Activity (DetailActivity) to come here

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001 && resultCode == RESULT_OK && data != null) {
            String name = data.getStringExtra("fullName");
            textView.setText(name);
        }
    }


}