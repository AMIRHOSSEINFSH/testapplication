package com.example.testapplication.first;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testapplication.R;

import java.util.ArrayList;

import static android.Manifest.permission.RECORD_AUDIO;
public class ThirdActivity extends AppCompatActivity {


    private SpeechRecognizer speechRecognizer;
    private Intent intentRecognizer;
    private TextView textView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        ActivityCompat.requestPermissions(this, new String[]{RECORD_AUDIO}, PackageManager.PERMISSION_GRANTED);
        textView = findViewById(R.id.textView);

        intentRecognizer = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intentRecognizer.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);

        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {
                //Toast.makeText(ThirdActivity.this, "g", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onBeginningOfSpeech() {
                //Toast.makeText(ThirdActivity.this, "h", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRmsChanged(float v) {
                //Toast.makeText(ThirdActivity.this, "k", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onBufferReceived(byte[] bytes) {
                //Toast.makeText(ThirdActivity.this, "l", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEndOfSpeech() {
                //Toast.makeText(ThirdActivity.this, ";", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(int i) {
                Toast.makeText(ThirdActivity.this, "Error Occurred!! "+ i, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResults(Bundle bundle) {
                Toast.makeText(ThirdActivity.this, "Result", Toast.LENGTH_SHORT).show();
                ArrayList<String> matches = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                String string = "";
                if(matches!=null) {
                    string = matches.get(0);
                    textView.setText(string);
                }
            }

            @Override
            public void onPartialResults(Bundle bundle) {
               // Toast.makeText(ThirdActivity.this, "3", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEvent(int i, Bundle bundle) {
                //Toast.makeText(ThirdActivity.this, "6", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void StartListener(View view){
        speechRecognizer.startListening(intentRecognizer);
    }
    public void StopListener(View view){
        speechRecognizer.stopListening();
    }

}