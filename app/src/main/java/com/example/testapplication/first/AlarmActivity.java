package com.example.testapplication.first;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;

import com.example.testapplication.R;

public class AlarmActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Todo bookmarks on lines 15,16 watch in Favorites tab
        setContentView(R.layout.activity_alarm);
        MediaPlayer mediaPlayer=MediaPlayer.create(this,R.raw.music1);
        mediaPlayer.start();
    }
}