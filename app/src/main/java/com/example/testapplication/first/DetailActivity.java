package com.example.testapplication.first;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.testapplication.R;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        final EditText editText=findViewById(R.id.et_editProfile);
        Button doneButton = findViewById(R.id.doneBotton);

        String fullName=getIntent().getStringExtra("fullName");
        editText.setText(fullName);


        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fullName=editText.getText().toString();
                Intent intent=new Intent(DetailActivity.this,secondActivity.class);
                intent.putExtra("fullName",fullName);
                setResult(RESULT_OK,intent);
                finish();
                MediaPlayer mediaPlayer=MediaPlayer.create(DetailActivity.this,R.raw.music1);
                mediaPlayer.start();
            }
        });

    }
}