package com.example.testapplication.Fragments_Project;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.testapplication.R;

import org.w3c.dom.Text;

public class MyFragment extends Fragment {



    /**
     * Todo Fragment LifeCycle
     1-onCreateView() --> fragmentView
     2-onViewCreated() --> getView()
     3-onActivityCreated() -->getContext()
     4-onStart() --> getActivity()
     */

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView=inflater.inflate(R.layout.fragment_my_fragment,container,false);
        //Todo fragmentView in here is Ok!
        TextView textView=fragmentView.findViewById(R.id.tv_myfragment);
        textView.setText("OnCreateView ...");
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Todo getView in here is Ok!
        TextView textView=getView().findViewById(R.id.tv_myfragment);
        textView.setText("OnViewCreated ...");
        Log.i("TAG", "onViewCreated: ");
    }

    @Override
    public void onStart() {
        super.onStart();
        //Todo getActivity() in here is Ok!
        TextView textView=getActivity().findViewById(R.id.tv_myfragment);
        textView.setText("OnStart ...");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Todo getContext() in here is Ok!
        Activity activity=(Activity)getContext();
        TextView textView=activity.findViewById(R.id.tv_myfragment);
        textView.setText("getActivityCreated ...");
    }
}
