package com.example.testapplication.Fragments_Project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.testapplication.R;

public class MainActivity_fragmentSection extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fragmentsetion);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_main_fragmentContainer,new fragmentA());
        fragmentTransaction.addToBackStack("FRAGMENT_A");
        fragmentTransaction.commit();

        Button btnRemove=findViewById(R.id.btn_main_remove_Fragment);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.frame_main_fragmentContainer);
                if (fragment!=null){
                    FragmentTransaction removeFragmentTransaction=getSupportFragmentManager().beginTransaction();
                    removeFragmentTransaction.remove(fragment);
                    removeFragmentTransaction.addToBackStack(null);
                    removeFragmentTransaction.commit();
                }

            }
        });

    }
}