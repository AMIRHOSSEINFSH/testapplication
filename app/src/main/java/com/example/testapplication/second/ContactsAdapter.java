package com.example.testapplication.second;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testapplication.R;

import java.util.ArrayList;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder> {


    ArrayList<String> contacts=new ArrayList<>();
    ItemEventListener itemEventListener;
    public ContactsAdapter(ItemEventListener itemEventListener){
        this.itemEventListener=itemEventListener;
        contacts.add("Ruthann Trustrie");
        contacts.add("Peadar Dawtrey");
        contacts.add("Felipe Bradtke");
        contacts.add("Claude Crissil");
        contacts.add("Jacky Girardeau");
        contacts.add("Rubia Dominguez");
        contacts.add("Michaela Churchley");
        contacts.add("Harvey Pentelow");
        contacts.add("Neilla Langton");
        contacts.add("Marco Greaves");
        contacts.add("Liz Batchley");
        contacts.add("Lamond Littlepage");
        contacts.add("Malina Weir");
        contacts.add("Tomlin Lenchenko");
        contacts.add("Hy Pavelin");
        contacts.add("Jenelle Palin");
        contacts.add("Damon Knewstubb");
        contacts.add("Alex Ivanusyev");
        contacts.add("Hamil Callery");
        contacts.add("Karol Syer");
    }







    public void addNewContact(String FullName){
        //TODO add FullName Argument to the ArrayList
        contacts.add(0,FullName);
        //TODO notify RecyclerView that Item has Inserted
        notifyItemInserted(0);
    }
    public void updateContact(String fullName,int position) {
        contacts.set(position,fullName);
        notifyItemChanged(position);

    }

    //Todo : call any time scroll but not repeated!! (At least items size)
    @NonNull
    @Override
    public ContactsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //TODO : attachToRoot Argument means if true Items are not Scrollable
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact,parent,false);
        return new ContactsViewHolder(view);
    }

    //TODO : OnBindViewHolder call any time we scroll down/up (Sea new items that we do not have it on page) with no limit
    //Todo any time scroll for any item even if repeated!!
    @Override
    public void onBindViewHolder(@NonNull ContactsViewHolder holder, int position) {
    holder.bindContact(contacts.get(position));
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }



    public class ContactsViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_firstcharacter,tv_fullname;

        public ContactsViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_firstcharacter=itemView.findViewById(R.id.tv_contact_firstCharachter);
            tv_fullname=itemView.findViewById(R.id.tv_contact_fullname);
        }
        public void bindContact(final String name){
            tv_firstcharacter.setText(name.charAt(0)+"");
            tv_fullname.setText(name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemEventListener.OnItemClick(name,getAdapterPosition());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    contacts.remove(getAdapterPosition());
                    //notifyDataSetChanged();
                    notifyItemRemoved(getAdapterPosition());
                    Log.i("TAGTAG", "onLongClick: "+contacts.size());
                    return false;
                }
            });

        }
    }

    public interface ItemEventListener{
        public void OnItemClick(String fullName,int position);
    }
}
