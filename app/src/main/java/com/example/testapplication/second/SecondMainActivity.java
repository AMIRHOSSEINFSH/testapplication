package com.example.testapplication.second;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.testapplication.R;

public class SecondMainActivity extends AppCompatActivity implements ContactsAdapter.ItemEventListener {

    private RecyclerView recyclerView;
    private ContactsAdapter contactsAdapter;
    //TODO editingItemPosition =if -1 is we are on adding statement or more than -1 we are on editing statement
    private int editingItemPosition=-1;
    private EditText editText;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_main);

        setUpViews();
        swipController swipController=new swipController();
        ItemTouchHelper itemTouchHelper=new ItemTouchHelper(swipController);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        //TODO reverseLayout Argument Stands for item classified form top to bottom(false) or Bottom to top (true)
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        contactsAdapter=new ContactsAdapter(this);
        recyclerView.setAdapter(contactsAdapter);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText.length()>0){

                    if (editingItemPosition==-1){
                        //Add new Contact
                        contactsAdapter.addNewContact(editText.getText().toString());
                        //Todo both Scroll to position 0 smooth scroll easily
                        //recyclerView.scrollToPosition(0);
                        recyclerView.smoothScrollToPosition(0);

                    }
                    else if (editingItemPosition>-1){
                        contactsAdapter.updateContact(editText.getText().toString(),editingItemPosition);
                        editingItemPosition=-1;
                        imageView.setImageResource(R.drawable.ic_add_white_24);
                    }

                    editText.setText("");
                }
            }
        });
    }

    private void setUpViews() {
        recyclerView=findViewById(R.id.rv_main);
        editText=findViewById(R.id.et_main_contactFullName);
        imageView=findViewById(R.id.iv_main_addNewContact);
    }

    @Override
    public void OnItemClick(String fullName, int position) {
        editingItemPosition=position;
        editText.setText(fullName);
        imageView.setImageResource(R.drawable.ic_done_white_24dp);

    }
}